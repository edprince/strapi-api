'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const { sanitizeEntity } = require('strapi-utils');

module.exports = {
  /**
   * Find records
   * @param {Object} ctx 
  async find(ctx) {
    const id = ctx.request.query.user;

    let entity;
    if (id) {
      let config = { 'user.id': id };
      entity = await strapi.query('post').find(config);
    } else {
      entity = await strapi.query('post').find();
    }

    return entity;
    //return sanitizeEntity(entity, { model: strapi.models.post });
  },
   */

   /**
   * Create a record.
   *
   * @return {Object}
   */
  async create(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data.user = ctx.state.user.id;
      entity = await strapi.services.post.create(data, { files });
    } else {
      ctx.request.body.user = ctx.state.user.id;
      entity = await strapi.services.post.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.post });
  },
   /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;

    const [post] = await strapi.services.post.find({
      id: ctx.params.id,
      'user.id': ctx.state.user.id,
    });

    if (!post) {
      return ctx.unauthorized(`You can't update this entry`);
    }

    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.post.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.post.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.post });
  },

  /**
   * Delete a record.
   * 
   * @return {Object}
   */
  async delete(ctx) {
    const { id } = ctx.params;

    let entity;

    const [post] = await strapi.services.post.find({
      id: ctx.params.id,
      'user.id': ctx.state.user.id,
    });

    if (!post) {
      return ctx.unauthorized(`You can't delete this entry`);
    }


    entity = await strapi.services.post.delete({ id });

    return sanitizeEntity(entity, { model: strapi.models.post });
  },
};
